const express = require("express");
const app = express();
const cors = require("cors");
// require("./src/db/conn");
require('dotenv').config();

const demoRouter = require("./src/controlllers/webhook");
const bodyParser = require("body-parser");
const url = 8000;


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
    next()
})
app.use('/api/v1',demoRouter)



app.listen(url, () => {
    console.log(`App is running on port ${url}`)
})

module.exports = app;
