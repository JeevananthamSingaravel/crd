const express = require("express")
const router = new express.Router()
// const fetch = require('axios');
const contentful = require('contentful-management')
// const cdaContentful = require('contentful')
const {getEventLogEntry,getContentType ,createEntryMappingEntry,getEntry ,makeObject ,calculateLocalePercentage ,getEntryMappingEntry} = require("../utilities/helpers")
const {cmaClient,globalSpaceToken} = require ("../utilities/constants")

/**
 * function for create entry in event log
 * @param {object} client 
 * @param {object} payload 
 * @param {object} res 
 * @param {string} entryTitle 
 */
const createEventLogEntry = (client,payload,res,entryTitle) => {
  try{
    console.log(payload.field[entryTitle],"chek")
    let title=""
    if(payload.field[entryTitle]){
      title = payload.field[entryTitle]["en-US"]
    }else{
      title = "Untitled"
    }
    client.getSpace('ov64r3ga08sj')
          .then((space) => space.getEnvironment('master'))
          .then((environment) => environment.createEntry('eventLog', {
            fields: {
              contentModelId: {
                'en-US': payload.contentType
              },
              entryId: {
                'en-US': payload.entryId
              },
              entryTitle:{
                "en-US":title
              }
            }
          }))
          .then((entry) => {
            entry.publish()
            createEntryMappingEntry(client,payload,res)
            
          })
  }catch(err){
    res.status(400)
    res.send({message:err})
  }

}

/**
 * funtion for updating event log entry
 * @param {object} client 
 * @param {object} payload 
 * @param {object} res 
 * @param {string} entryTitle 
 * @param {string} entryId 
 */
const updateEventLogEntry = async(client,payload,res,entryTitle,entryId) => {
  console.log(payload.field[entryTitle]["en-US"],"update event payload",entryId)
  try{
    let title=""
    if(payload.field[entryTitle]["en-US"]){  
    const entry = await getEntry(payload)
    // const keys = Object.keys(entry.fields)
    // const localeString = payload.field[keys[0]]
    // const locale =JSON.stringify(localeString).split(":")[0]
    // const newLocale = locale.split('"')[1]
    console.log(payload,"content type payload")
    // for(let i=0;i<keys.length;i++){
    //   let key = keys[i]
    //   if(entry.fields[key] != payload.field[key][newLocale]){
    //     console.log(entry.fields[key],payload.field[key][newLocale],"update entry mapping")
    //     updateEntryMapping(newLocale,payload.entryId,res)
    //     break
    //   }
    //   console.log("its not false")
    // }
    console.log(entry,"locale update entry")
      title = payload.field[entryTitle]["en-US"]
    }else{
      title = "Untitled"
    }

    client.getSpace('ov64r3ga08sj')
    .then((space) => space.getEnvironment('master'))
    .then((environment) => environment.getEntry(entryId))
    .then(async (entry) => {
      entry.fields.entryTitle['en-US'] = title
      const response = await entry.update()
      await response.publish()
    }).then((entry)=>{
      res.send({message:"entry updated"})
    })
  }catch(err){
    console.log(err)
    res.status(400)
    res.send({error:err})
  }

}

/**
 * api for creating entry in locale spaces
 */
router.post("/createLocaleEntry",async(req,res)=>{
  try{
    console.log(req.body,"locale entry creation called")
    const payloadArray = req.body.spaces
    const response = await getEntry(req.body)
    console.log(response,"ressss")
    for(let i=0;i<payloadArray.length;i++){
      let payload = payloadArray[i]
      let fields = await makeObject(response.fields,payload.localeSpaceName)
      const client = contentful.createClient({
          accessToken: payload.accessToken
      })
      const space = await client.getSpace(payload.localeSpaceId)
      const env = await space.getEnvironment("master")
      const entry = await env.createEntryWithId(req.body.contentType,response.sys.id,{fields})
      await entry.publish()
      const entryMappingEntryResponse = await getEntryMappingEntry(req.body.entryId)
      const entryMappingEntryResponseJson = await entryMappingEntryResponse.json()
      const entryMappingEntryId  = entryMappingEntryResponseJson.data.entryMappingCollection.items[0]
      const entryMappingEntry =await getEntry({entryId:entryMappingEntryId.sys.id})
      if(!(entryMappingEntry.fields.localizedEntryFlags[payload.localeSpaceName])){
        let localizedEntryFlag = entryMappingEntry.fields.localizedEntryFlags
        let childObject = {}
        let contentType = await getContentType({contentType:entryMappingEntry.fields.modelName},res)
        contentType.fields.forEach((element)=>{
          if(element.localized){
            childObject[element.id] = 100
          }
        })
        localizedEntryFlag[payload.localeSpaceName] = childObject
        await updateEntryMapping(payload.localeSpaceName,req.body.entryId,res,childObject,true)
        console.log(payload.localeSpaceName,req.body.entryId,childObject,true,"entry mapping entry")
      }
      
      
    }
    res.send({message:"entry created succesfull"})
  }catch(err){
    console.log(err,"error")
    res.status(400)
    res.send({message:err})
  }
})

/**
 * api for creating space mapping entry in global
 */
router.post("/createLocale",async(req,res)=>{
  try{
    const payload = req.body
    const client = contentful.createClient({
      accessToken: globalSpaceToken
    })
    const response = await client.getSpace('ov64r3ga08sj')
    .then((space) => space.getEnvironment('master'))
    .then((environment) => environment.createEntry('spaceMapping', {
      fields: {
        localeSpaceName: {
          'en-US': payload.name
        },
        localeSpaceId: {
          'en-US': payload.id
        },
        environmentId: {
          'en-US': payload.environmentId
        },
        accessToken: {
          'en-US': payload.accessToken
        },
      }
    }))
    .then(async(entry) => {
      await entry.publish()
      res.send({message:"entry created successfull",data:entry})
    })  
  }
  catch(err){
    res.status(400)
    res.send({message:err})
  }
})

/**
 * api for create event log entry in global space
 */
router.post("/createEventLogEntry",async(req,res)=>{
  try{
    const payload = req.body
    if(payload.contentType != "eventLog" && payload.contentType != "spaceMapping" && payload.contentType != "entryMapping"){
    const client = contentful.createClient({
      accessToken: globalSpaceToken
    })
    const status =await getEventLogEntry(payload.entryId)  
    const contentTypeDetail =await getContentType(payload,res)
    console.log(contentTypeDetail,"details");
    if(!status){
      console.log("get content")
      createEventLogEntry(client,payload,res,contentTypeDetail.displayField)
    }else{
      console.log(status,"status")
      updateEventLogEntry(client,payload,res,contentTypeDetail.displayField,status)
    }
    }
  }
  catch(err){
    console.log(err,"errr")
    res.status(400)
    res.send({message:err})
  }
})

/**
 * api for getting entry from eventlog model in global space
 */
router.get("/getEventLogEntries",async(req,res)=> {
  try{
    const query = `query {
      eventLogCollection{
        items{
          contentModelId
          entryId
          entryTitle
        }
      }
    }`
    const eventLogList =await fetch("https://graphql.contentful.com/content/v1/spaces/ov64r3ga08sj?access_token=eCA_T4CqDY8bM5jKqigY48DXMDKUOG9jXvlov0nxbUQ",{
          method:"POST",
          headers:{
            "Content-Type":"application/json",
          },
          body:JSON.stringify({query})
    })
    const response = await eventLogList.json()
    if(response.data.eventLogCollection.items.length > 0){
      res.send({data:response.data.eventLogCollection.items}) 
    }else{
      res.send({data:[]})
    }
  }catch(err){
    res.status(400)
    res.send({message:err})
  }
})

/**
 * api for getting entry from space mapping model in global space
 */
router.get("/getSpaceMappingEntries",async(req,res)=>{
  try{
    console.log("space mapping entry called")
    const query = `query {
      spaceMappingCollection{
        items{
          localeSpaceId
          localeSpaceName
          accessToken
        }
      }
    }`
    
    const spaceMappingList = await fetch("https://graphql.contentful.com/content/v1/spaces/ov64r3ga08sj?access_token=eCA_T4CqDY8bM5jKqigY48DXMDKUOG9jXvlov0nxbUQ",{
      method:"POST",
      headers:{
        "Content-Type":"application/json",
      },
      body:JSON.stringify({query})
    }) 
    console.log(spaceMappingList,"space")
    const response = await spaceMappingList.json()
    const spaceList = response.data.spaceMappingCollection.items
    const entryList = await getEntryMappingList()
    console.log(entryList,"entry list")
    let parentObject = {}
    spaceList.forEach((space) => {
      parentObject[space.localeSpaceName] = []
    })
    
    console.log(parentObject,"parent")
    entryList.forEach((entry) => {
      const keys = Object.keys(entry.localizedEntryFlags)
      keys.forEach((locale) => {
        console.log(entry.localizedEntryFlags[locale],"objects",locale)
        let field = entry.localizedEntryFlags[locale]
        const fieldKeys = Object.keys(entry.localizedEntryFlags[locale])
        let global= 0
        // let temporaryArr = []
        fieldKeys.forEach((fieldType)=>{
          global = field[fieldType] + global
        })
        let childObject = {
          global:global/fieldKeys.length,
          local:100 - (global/fieldKeys.length)
        }
        console.log(childObject,"child")
        parentObject[locale].push(childObject)
        console.log(parentObject[locale],"parentObject[locale]")

      });
    });
    console.log(parentObject,"parent Object")
 
     spaceList.forEach((data)=> {
      data["global"] = calculateLocalePercentage(parentObject[data.localeSpaceName])
      data["local"] = 100 - data.global
      switch(data.localeSpaceName){
        case "es-VE": 
        return data["label"]= "Content Syndication - ES (Spanish)";
        case "fr":
        return data["label"]= "Content Syndication - FR (French)";
        case "de":
        return data["label"]= "Content Syndication - DE (German)";
        case "sv":
        return data["label"]= "Content Syndication - SV (Swedish)";
        default:
        return data["label"]= data.localeSpaceName;


      }
    })
    console.log(spaceList,"after")
    res.send({data:spaceList})
  }catch(err){
    // console.log(err.response.data.errors,"eroor");
    res.status(400)
    res.send({message:err})
  }
})

/**
 * api for locale space entry update 
 */
router.post("/localeEntryUpdate",async(req,res)=>{
  try{
    console.log("calling",req.body)
    const payload = req.body
    const entry = await getEntry(payload)
    const contentModelDetail = await getContentType(payload,res)
    let keys = []
    contentModelDetail.fields.forEach((element)=> {
      if(element.localized){
        keys.push(element.id)
      }
    })

    let localizedEntryFlag = {}
    const localeString = payload.field[keys[0]]
    const locale =JSON.stringify(localeString).split(":")[0]
    const newLocale = locale.split('"')[1]
    let changeStatus = false
    for(let i=0;i<keys.length;i++){
      let key = keys[i]
      if(entry.fields[key] != payload.field[key][newLocale]){
        localizedEntryFlag[key]=0 
        changeStatus=true
        console.log(localizedEntryFlag,keys,"update entry mapping")
      }else{
        localizedEntryFlag[key]=100 
      }
      console.log("its not false")
    }

    console.log(localizedEntryFlag,"flagg")
      // if(changeStatus){
        console.log(localizedEntryFlag,"new flag")
        updateEntryMapping(newLocale,payload.entryId,res,localizedEntryFlag,false)
      // }
    console.log(entry,"locale update entry")
  }catch(err){
    res.status(400)
    res.send({message:err})
  }

})

router.get("/getGlobalEntry/:id",async(req,res)=>{
  try{
    console.log("req", req.params.id)
    const globalEntry = await fetch(`https://cdn.contentful.com/spaces/ov64r3ga08sj/environments/master/entries/${req.params.id}?access_token=eCA_T4CqDY8bM5jKqigY48DXMDKUOG9jXvlov0nxbUQ`,{
      method:"GET",
      headers:{
        "Content-Type":"application/json",
      },
     }) 
    const response = await globalEntry.json()
     res.send({data:response})
  }
  catch(err){
    res.status(400)
    res.send({message:err})
  }
})

//update entry mapping JSON
/**
 * 
 * @param {string} locale 
 * @param {string} entryId 
 * @param {object} res 
 * @param {object} updatedJson 
 */
const updateEntryMapping =async (locale,entryId,res,updatedJson,status) => {
  // console.log(cmaClient,"client")
try{
  console.log(locale,entryId,updatedJson,status,"checkiiiiii")
  const response = await getEntryMappingEntry(entryId)
  const data =await response.json()
  console.log(data.data.entryMappingCollection.items[0],"update entry mapping data")
  let id=""
  if(data){
    id = data.data.entryMappingCollection.items[0].sys.id
    const spaceMap = await cmaClient.getSpace('ov64r3ga08sj')
    .then((space) => space.getEnvironment('master'))
    .then((environment) => environment.getEntry(id))
    .then(async(entry) => {
      console.log(entry,"newwww entry")
      entry.fields.localizedEntryFlags['en-US'][locale] = updatedJson
      const update = await entry.update()
      const publish = await update.publish()
      if(!status){
        res.send({message:"entry updated"})
      }      
    })
  }
  console.log(id,"odddddd")
}
catch(err){
  console.log(err)
  res.status(400)
  res.send({message:err})
}
  

}

/**
 * function for get the entry mapping localization flag in global space
 * @returns {object[]} entry mapping localized flag filed values
 */
const getEntryMappingList = async() => {
  const query = `query {
    entryMappingCollection{
      items{
        localizedEntryFlags
      }
    }
  }`

  const response = await fetch("https://graphql.contentful.com/content/v1/spaces/ov64r3ga08sj?access_token=eCA_T4CqDY8bM5jKqigY48DXMDKUOG9jXvlov0nxbUQ",{
    method:"POST",
    headers:{
      "Content-Type":"application/json",
    },
    body:JSON.stringify({query})
  }) 

  const entryMappingList = await response.json()
  return entryMappingList.data.entryMappingCollection.items

}
 
module.exports = router