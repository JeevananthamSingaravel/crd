const contentful = require('contentful')
const cmaContentful = require("contentful-management")
const { response } = require('express')
const {cdaAccessToken} = require("./constants")
// const fetch = require("axios")

//create client for api calling
const createClient = async(spaceId,environMent,accessToken) => {
    const client =await contentful.createClient({
        space: spaceId,
        environment:environMent , // defaults to 'master' if not set
        accessToken: accessToken
    })
    return client
}


/**
 * get single entry from global space
 * @param {object} entryDetail 
 * @returns {object} single entry 
 */
const getEntry =async (entryDetail) => {
  console.log("get entry called")
  const client =await createClient("ov64r3ga08sj","master",cdaAccessToken)
  const entry = await client.getEntry(entryDetail.entryId)
.catch(console.error)

console.log(entry,entryDetail , "entry")
return entry
}

/**
 * get a entry Id from eventlog content model in global space
 * @param {string} entryId 
 * @returns {string} id of that entry
 */
const getEventLogEntry =async (entryId) => {  
  const query = `query{
      eventLogCollection(where:{entryId:"${entryId}"}){
        items{
          sys{
            id
          }
        }
      }
    }`
    
  const entryStatus =await fetch("https://graphql.contentful.com/content/v1/spaces/ov64r3ga08sj?access_token=eCA_T4CqDY8bM5jKqigY48DXMDKUOG9jXvlov0nxbUQ",{
      method:"POST",
      headers:{
        "Content-Type":"application/json",
      },
      body:JSON.stringify({query})
    })
  const response =await entryStatus.json()

  if(response.data.eventLogCollection.items.length > 0){
      return response.data.eventLogCollection.items[0].sys.id
  }
  return false
}

const createDeliveryClient = (accessToken) => {
    const client = contentful.createClient({
        accessToken: accessToken     
    });
    return client
}


//get the all the locale name 
const getAllSpaceName = async() => {
  const query = `query {
    spaceMappingCollection{
      items{
        localeSpaceName
      }
    }
  }`
  const spaceList =await fetch("https://graphql.contentful.com/content/v1/spaces/ov64r3ga08sj?access_token=eCA_T4CqDY8bM5jKqigY48DXMDKUOG9jXvlov0nxbUQ",{
        method:"POST",
        headers:{
          "Content-Type":"application/json",
        },
        body:JSON.stringify({query})
      })
  
  const response = await spaceList.json()

  if(response.data.spaceMappingCollection.items.length > 0)
    return response.data.spaceMappingCollection.items
  return []
}
 
//create entry in entry mapping
const createEntryMappingEntry =async (client,payload,res) => {
  const contentTypeDetail = await getContentType(payload,res)
  console.log(contentTypeDetail,"detail")
  const spaceList =await getAllSpaceName()
  console.log(spaceList,"entry map called")
  let flag = {}
  let localizedFieldFlag = {}
  contentTypeDetail.fields.forEach((element)=>{
    if(element.localized){
      localizedFieldFlag[element.id] = 100
    }
  })
  console.log(localizedFieldFlag,"local flags")
  spaceList.forEach((element) => {
    flag[element.localeSpaceName]=localizedFieldFlag
  });

  console.log(flag,"flag")
  client.getSpace('ov64r3ga08sj')
    .then((space) => space.getEnvironment('master'))
    .then((environment) => environment.createEntry('entryMapping', {
      fields: {
        modelName: {
          'en-US': payload.contentType
        },
        entryId: {
          'en-US': payload.entryId
        },
        localizedEntryFlags:{
          'en-US' :flag
        }
      }
    }))
    .then((entry) => {
      entry.publish()
      console.log(entry,"entry")
      res.send({message:"entry created successfull",data:entry})
    })
}

const makeObject = (data,locale) => {
  let temporaryObject = {}
  let localeObject = {}
  let keys = Object.keys(data)
  keys.forEach((key)=>{
    localeObject={}
    localeObject[locale]=data[key]
    temporaryObject[key]=localeObject
  })
  console.log(temporaryObject,"tempoooo object")
  return temporaryObject
}

//get the content type
const getContentType = async(payload,res) => {
  console.log(payload,"payyyyyy")
  try{
    const client = contentful.createClient({
      space: 'ov64r3ga08sj',
      environment: 'master', // defaults to 'master' if not set
      accessToken: cdaAccessToken
    })
    const contentType = await client.getContentType(payload.contentType)
    return contentType
  }catch(err){
    res.status(400)
    res.send({error:err})
  }
}

const calculateLocalePercentage = (data) => {
  console.log(data,"data")
  let global = 0 
  data.forEach((element)=> {
    global = global + element.global
  })
  console.log(global/data.length)
  return global/data.length
} 

const getEntryMappingEntry = async(entryId) => {
  const query = `query {
    entryMappingCollection(where:{entryId:"${entryId}"}){
      items{
        sys{
          id
        }
      }
    }
  }`

  console.log(query,"queryyy");

  const response =await fetch("https://graphql.contentful.com/content/v1/spaces/ov64r3ga08sj?access_token=eCA_T4CqDY8bM5jKqigY48DXMDKUOG9jXvlov0nxbUQ",{
    method:"POST",
    headers:{
      "Content-Type":"application/json",
    },
    body:JSON.stringify({query})
  })

  return response
}
module.exports ={
    createClient,
    getEventLogEntry,
    createDeliveryClient,
    createEntryMappingEntry,
    getEntry,
    makeObject,
    getContentType,
    calculateLocalePercentage,
    getEntryMappingEntry
}