const responseFormatter = (res, error, data) => {
    if (error) {
      res.status(400);
      res.send(error);
    } else {
      Promise.resolve(data).then((value) => {
        if (!value) {
          res.status(204);
          res.send({ data: null });
        } else {
          const response = data;
          res.status(200);
          res.send(response);
        }
      })
    }
  };

  module.exports = { responseFormatter }
  